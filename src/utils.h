#ifndef UTILS_H_
#define UTILS_H_

#include <stdlib.h>
#include <stdnoreturn.h>
#include <math.h>

#define LIKELY(x)   __builtin_expect(!!(x), 1)
#define UNLIKELY(x) __builtin_expect(!!(x), 0)

noreturn void report_error(const char* format, ...);
void show_fps();

// https://godbolt.org/z/dTYxxoxnW
static inline float clamp(float x, float min, float max)
{
    const float t = x < min ? min : x;
    return t > max ? max : t;
}

static inline float distance(float x1, float y1, float x2, float y2)
{
    return (x1 - x2) * (x1 - x2) + (y1 - y2) * (y1 - y2);
}

static inline int randint(int min, int max)
{
   return min + rand() % (max + 1 - min);
}

static inline float randf(float min, float max)
{
    return min + (max - min) * rand() / (float)RAND_MAX;
}

static inline float fdiv(float dividend, float divider)
{
    return floorf(dividend / divider) * divider;
}

static inline float square(float x)
{
    return x * x;
}

#endif  // UTILS_H_
