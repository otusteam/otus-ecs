#include <stdbool.h>
#include <stdlib.h>
#include <allegro5/allegro.h>
#include <allegro5/allegro_acodec.h>
#include <allegro5/allegro_image.h>
#include <allegro5/allegro_primitives.h>
#include <allegro5/allegro_ttf.h>
#include "components.h"
#include "systems.h"
#include "utils.h"


int main(int argc, char** argv)
{
    srand(time(NULL));

    // init liballegro
    if(!al_init())
    {
        report_error("Allegro initialization failed");
    }
    atexit(al_uninstall_system);
    if(!al_init_primitives_addon())
    {
        report_error("Allegro primitives addon initialization failed");
    }
    if(!al_init_image_addon())
    {
        report_error("Allegro image addon initialization failed");
    }
    if(!al_init_font_addon())
    {
        report_error("Allegro font addon initialization failed");
    }
    if(!al_init_ttf_addon())
    {
        report_error("Allegro TTF addon initialization failed");
    }
    if(!al_install_audio())
    {
        report_error("Allegro audio system initialization failed");
    }
    if(!al_init_acodec_addon())
    {
        report_error("Allegro audio codec initialization failed");
    }
    if(!al_restore_default_mixer())
    {
        report_error("Allegro audio mixer initialization failed");
    }

    al_set_new_display_option(ALLEGRO_VSYNC, 1, ALLEGRO_SUGGEST);
    ALLEGRO_DISPLAY* display = al_create_display(SCREEN_WIDTH, SCREEN_HEIGHT);
    if(!display)
    {
        report_error("Display initialization failed");
    }
    ALLEGRO_EVENT_QUEUE* queue = al_create_event_queue();
    if(!queue)
    {
        report_error("Event queue initialization failed");
    }
    al_register_event_source(queue, al_get_display_event_source(display));
    al_install_keyboard();
    al_install_mouse();
    al_install_joystick();
    al_register_event_source(queue, al_get_joystick_event_source());

    // init flecs
#ifndef NDEBUG
    ecs_log_set_level(0);
#endif
    ecs_world_t* world = ecs_init_w_args(argc, argv);
#ifndef NDEBUG
    // https://flecs.dev/explorer
    ecs_singleton_set(world, EcsRest, {0});
    ECS_IMPORT(world, FlecsMonitor);
#endif
    register_components(world);
    ecs_plecs_from_file(world, argc == 2 ? argv[1] : ASSETS_PATH "scene.plecs");
    register_systems(world);

    // main loop
    bool running = true;
    while(running)
    {
        ALLEGRO_EVENT event;
        while(al_get_next_event(queue, &event))
        {
            if(UNLIKELY(event.type == ALLEGRO_EVENT_DISPLAY_CLOSE))
                running = false;
            else if(UNLIKELY(event.type == ALLEGRO_EVENT_JOYSTICK_CONFIGURATION))
                al_reconfigure_joysticks();
        }

        al_clear_to_color(al_map_rgb(0, 0, 0));

        if(UNLIKELY(!ecs_progress(world, 0)))
            running = false;

#ifndef NDEBUG
        show_fps();
#endif
        al_flip_display();
    }

    // cleanup
    ecs_fini(world);
    al_uninstall_keyboard();
    al_uninstall_mouse();
    al_destroy_event_queue(queue);
    al_destroy_display(display);

    return EXIT_SUCCESS;
}
