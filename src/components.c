#define COMPONENTS_IMPL
#include "components.h"


ECS_DECLARE(SingleSound);
ECS_DECLARE(LoopingSound);
ECS_DECLARE(Player);
ECS_DECLARE(Enemy);

void register_components(ecs_world_t* world)
{
    ECS_META_COMPONENT(world, Position);
    ECS_META_COMPONENT(world, Size);
    ECS_META_COMPONENT(world, Bitmap);
    ECS_META_COMPONENT(world, Sprite);
    ECS_META_COMPONENT(world, Animation);
    ECS_META_COMPONENT(world, SoundData);
    ECS_META_COMPONENT(world, SoundInstance);
    ECS_META_COMPONENT(world, Velocity);
    ECS_META_COMPONENT(world, MaxVelocity);
    ECS_META_COMPONENT(world, Thrust);
    ECS_META_COMPONENT(world, MaxThrust);
    ECS_META_COMPONENT(world, ExhaustSprite);
    ECS_META_COMPONENT(world, Angle);
    ECS_META_COMPONENT(world, MaxTurnSpeed);
    ECS_META_COMPONENT(world, Background);
    ECS_META_COMPONENT(world, Font);
    ECS_META_COMPONENT(world, Text);
    ECS_META_COMPONENT(world, Hull);
    ECS_META_COMPONENT(world, Shield);
    ECS_META_COMPONENT(world, ShieldSprite);
    ECS_META_COMPONENT(world, Lifetime);
    ECS_META_COMPONENT(world, Cooldown);
    ECS_META_COMPONENT(world, Damage);
    ECS_META_COMPONENT(world, AI);
    ECS_META_COMPONENT(world, Fleet);

    ECS_TAG_DEFINE(world, SingleSound);
    ECS_TAG_DEFINE(world, LoopingSound);
    ECS_TAG_DEFINE(world, Player);
    ECS_TAG_DEFINE(world, Enemy);
}
