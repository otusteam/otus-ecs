#ifndef SYSTEMS_H_
#define SYSTEMS_H_

#include "flecs.h"

#define SCREEN_WIDTH 800
#define SCREEN_HEIGHT 600
#define SCREEN_SCALE 4
#define ASSETS_PATH "../Resources/"

void register_systems(ecs_world_t* world);

#endif  // SYSTEMS_H_
