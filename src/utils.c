#include <stdarg.h>
#include <stdio.h>
#include <stdlib.h>
#include <allegro5/allegro_font.h>
#include <allegro5/allegro_native_dialog.h>
#include "utils.h"

noreturn void report_error(const char* format, ...)
{
    char str[64];
    va_list args;

    va_start(args, format);
    vsnprintf(str, sizeof(str), format, args);
    va_end(args);

    if(al_init_native_dialog_addon())
    {
        ALLEGRO_DISPLAY* display =
            al_is_system_installed() ? al_get_current_display() : NULL;
        al_show_native_message_box(
            display, "Hey guys", "We got a big error here :(", str, NULL, 0);
    }
    else
    {
        fputs(str, stderr);
    }
    exit(EXIT_FAILURE);
}

#define TIMESTAMPS_COUNT 60
static double timestamps[TIMESTAMPS_COUNT];
static size_t current_timetamp;

void show_fps()
{
    timestamps[current_timetamp++] = al_get_time();
    if(current_timetamp >= TIMESTAMPS_COUNT)
        current_timetamp = 0;

    double sum = 0;
    for(size_t i = 0, prev = TIMESTAMPS_COUNT - 1; i < TIMESTAMPS_COUNT; i++)
    {
        if(i != current_timetamp)
        {
            double dt = timestamps[i] - timestamps[prev];
            sum += dt;
        }
        prev = i;
    }
    double avg = sum / (TIMESTAMPS_COUNT - 1);
    unsigned fps = 1.0 / avg;

    static ALLEGRO_FONT* font;
    if(!font)
        font = al_create_builtin_font();

    char buffer[8];
    snprintf(buffer, sizeof(buffer), "%3d FPS", fps);
    al_draw_text(font, al_map_rgb(255, 255, 255), 0, 0, 0, buffer);
}
