#ifndef COMPONENTS_H_
#define COMPONENTS_H_

#include <allegro5/allegro.h>
#include <allegro5/allegro_audio.h>
#include <allegro5/allegro_font.h>
#include "flecs.h"

#undef ECS_META_IMPL
#ifndef COMPONENTS_IMPL
#define ECS_META_IMPL EXTERN
#endif

ECS_STRUCT(
    Position,
    {
        float x;
        float y;
    });

ECS_STRUCT(
    Size,
    {
        const float width;
        const float height;
    });

ECS_STRUCT(
    Bitmap,
    {
        const char* name;
        ALLEGRO_BITMAP * bitmap;
    });

ECS_STRUCT(
    Sprite,
    {
        float offset_x;
        float offset_y;
    });

ECS_STRUCT(
    Animation,
    {
        const uint32_t length;
        const float interval;
        const uint32_t first_frame_x;
        float time;
        uint32_t frame;
    });

ECS_STRUCT(
    SoundData,
    {
        const char* name;
        ALLEGRO_SAMPLE * sample;
    });

ECS_STRUCT(
    SoundInstance,
    {
        ALLEGRO_SAMPLE_INSTANCE * instance;
        float time;
        float length;
        bool playing;
        float gain;
        float pan;
    });

extern ECS_DECLARE(SingleSound);
extern ECS_DECLARE(LoopingSound);

ECS_STRUCT(
    Velocity,
    {
        float x;
        float y;
    });

ECS_STRUCT(
    MaxVelocity,
    {
        const float max;
    });

ECS_STRUCT(
    Thrust,
    {
        float force;
    });

ECS_STRUCT(
    MaxThrust,
    {
        const float max;
    });

ECS_STRUCT(
    ExhaustSprite,
    {
        const uint32_t levels;
    });

ECS_STRUCT(
    Angle,
    {
        float angle;
    });

ECS_STRUCT(
    MaxTurnSpeed,
    {
        const float max;
    });


extern ECS_DECLARE(Player);
extern ECS_DECLARE(Enemy);

ECS_STRUCT(
    Background,
    {
        const float parallax;
    });

ECS_STRUCT(
    Font,
    {
        const char* name;
        const int32_t size;
        ALLEGRO_FONT * font;
    });

ECS_STRUCT(
    Text,
    {
        char* string;
    });

ECS_STRUCT(
    Hull,
    {
        const float max_durability;
        float durability;
    });

ECS_STRUCT(
    Shield,
    {
        const float max_strength;
        const float regeneration;
        float strength;
    });

ECS_STRUCT(
    ShieldSprite,
    {
        const uint8_t levels;
        const uint8_t start_x;
    });

ECS_STRUCT(
    Lifetime,
    {
        float left;
    });

ECS_STRUCT(
    Cooldown,
    {
        const float length;
        float current;
    });

ECS_STRUCT(
    Damage,
    {
        const uint32_t min;
        const uint32_t max;
    });

ECS_STRUCT(
    AI,
    {
        const float notice_distance;
        const float target_distance;
        const float shoot_tolerance;
        const float courage;
    });

ECS_STRUCT(
    Fleet,
    {
        const uint32_t size;

        const float width;
        const float height;

        const float offset_x_min;
        const float offset_x_max;
        const float offset_y_min;
        const float offset_y_max;

        const float pos_x_min;
        const float pos_x_max;
        const float pos_y_min;
        const float pos_y_max;

        const float vel_x_min;
        const float vel_x_max;
        const float vel_y_min;
        const float vel_y_max;

        const float max_vel_min;
        const float max_vel_max;

        const float max_thrust_min;
        const float max_thrust_max;

        const float max_turn_speed_min;
        const float max_turn_speed_max;

        const float angle_min;
        const float angle_max;

        const float max_hull_min;
        const float max_hull_max;

        const float hull_min;
        const float hull_max;

        const float shield_min;
        const float shield_max;

        const float shield_regen_min;
        const float shield_regen_max;

        const float shield_sprite_x;
        const float shield_sprite_y;

        const float cooldown_min;
        const float cooldown_max;

        const float exhaust_sprite_x;
        const float exhaust_sprite_y;

        const uint32_t exhaust_anim_len;
        const float exhaust_anim_interval;

        const float notice_distance_min;
        const float notice_distance_max;
        const float target_distance_min;
        const float target_distance_max;
        const float shoot_tolerance_min;
        const float shoot_tolerance_max;
        const float courage_min;
        const float courage_max;
    });

void register_components(ecs_world_t* world);

#endif  // COMPONENTS_H_
