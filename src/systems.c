#include <float.h>
#include <math.h>
#include <stdio.h>
#include <allegro5/allegro_primitives.h>
#include <allegro5/allegro_ttf.h>
#include "flecs.h"
#include "components.h"
#include "systems.h"
#include "utils.h"


#define DELETE_SYSTEM(world, system) {           \
    ecs_entity_t s = ecs_lookup(world, #system); \
    if(s) ecs_delete(world, s); }

static void GenerateFleet(ecs_iter_t* it)
{
    const Fleet* fleet = ecs_field(it, Fleet, 1);

    ecs_world_t* world = it->world;

    ecs_entity_t ship = ecs_lookup(world, "Ship");
    ecs_assert(ship, ECS_MISSING_SYMBOL, NULL);
    ecs_entity_t shield_prefab = ecs_lookup(world, "ShieldEffect");
    ecs_assert(shield_prefab, ECS_MISSING_SYMBOL, NULL);
    ecs_entity_t exhaust_prefab = ecs_lookup(world, "EnemyExhaustEffect");
    ecs_assert(exhaust_prefab, ECS_MISSING_SYMBOL, NULL);
    for(int i = 0; i < it->count; i++)
    {
        float
            width  = fleet[i].width,
            height = fleet[i].height;
        for(unsigned j = 0; j < fleet[i].size; j++)
        {
            ecs_entity_t enemy = ecs_new_w_pair(world, EcsIsA, ship);
            ecs_add(world, enemy, Enemy);
            ecs_set(world, enemy, Size, {width, height});
            ecs_set(world, enemy, Sprite, {
                    fdiv(randf(fleet[i].offset_x_min, fleet[i].offset_x_max),
                         width),
                    fdiv(randf(fleet[i].offset_y_min, fleet[i].offset_y_max),
                         height)});
            ecs_set(world, enemy, Position, {
                    randf(fleet[i].pos_x_min, fleet[i].pos_x_max),
                    randf(fleet[i].pos_y_min, fleet[i].pos_y_max)});
            ecs_set(world, enemy, Velocity, {
                    randf(fleet[i].vel_x_min, fleet[i].vel_x_max),
                    randf(fleet[i].vel_y_min, fleet[i].vel_y_max)});
            ecs_add(world, enemy, Thrust);
            ecs_set(world, enemy, MaxVelocity, {
                    randf(fleet[i].max_vel_min, fleet[i].max_vel_max)});
            ecs_set(world, enemy, MaxThrust, {
                    randf(fleet[i].max_thrust_min, fleet[i].max_thrust_min)});
            ecs_set(world, enemy, MaxTurnSpeed, {
                    randf(fleet[i].max_turn_speed_min,
                          fleet[i].max_turn_speed_max)});
            ecs_set(world, enemy, Angle, {
                    randf(fleet[i].angle_min, fleet[i].angle_max)});
            float hull = randf(fleet[i].max_hull_min, fleet[i].max_hull_max);
            ecs_set(world, enemy, Hull, {
                    hull,
                    fmin(hull, randf(fleet[i].hull_min, fleet[i].hull_max))});
            ecs_set(world, enemy, Shield, {
                    randf(fleet[i].shield_min, fleet[i].shield_max),
                    randf(fleet[i].shield_regen_min, fleet[i].shield_regen_max),
                    0});
            ecs_set(world, enemy, Cooldown, {
                    randf(fleet[i].cooldown_min, fleet[i].cooldown_max), 0});
            ecs_set(world, enemy, AI, {
                    randf(fleet[i].notice_distance_min,
                          fleet[i].notice_distance_max),
                    randf(fleet[i].target_distance_min,
                          fleet[i].target_distance_max),
                    randf(fleet[i].shoot_tolerance_min,
                          fleet[i].shoot_tolerance_max),
                    randf(fleet[i].courage_min,
                          fleet[i].courage_max)});

            ecs_entity_t shield = ecs_new_w_pair(world, EcsIsA, shield_prefab);
            ecs_add_pair(world, shield, EcsChildOf, enemy);
            ecs_set(world, shield, Sprite, {
                    fleet[i].shield_sprite_x, fleet[i].shield_sprite_y});
            ecs_add(world, shield, SoundInstance);

            ecs_entity_t exhaust = ecs_new_w_pair(
                world, EcsIsA, exhaust_prefab);
            ecs_add_pair(world, exhaust, EcsChildOf, enemy);
            ecs_set(world, exhaust, Sprite, {
                    fleet[i].exhaust_sprite_x, fleet[i].exhaust_sprite_y});
            ecs_set(world, exhaust, Animation, {
                    fleet[i].exhaust_anim_len, fleet[i].exhaust_anim_interval,
                    fleet[i].exhaust_sprite_x, 0, 0});
            ecs_add(world, exhaust, Position);
            ecs_add(world, exhaust, Angle);
            ecs_add(world, exhaust, SoundInstance);
        }
    }
    DELETE_SYSTEM(world, GenerateFleet);
}

static void DecayPlayerThrust(ecs_iter_t* it)
{
    Thrust* thrust = ecs_field(it, Thrust, 1);
    const MaxThrust* max_thrust = ecs_field(it, MaxThrust, 2);

    float dt = it->delta_time;
    for(int i = 0; i < it->count; i++)
    {
        thrust[i].force = clamp(
            thrust[i].force - max_thrust[i].max * dt,
            0, max_thrust[i].max);
    }
}

#define SCREEN_SCALE_SQUARED (SCREEN_SCALE * SCREEN_SCALE)
#define SCREEN_AREA (SCREEN_WIDTH * SCREEN_HEIGHT)

static void position_sound(SoundInstance* sound, const Position* pos,
                           const Position* player_pos)
{
    float
        x = pos->x,
        y = pos->y,
        px = player_pos->x,
        py = player_pos->y;
    sound->gain = clamp(
        1 - distance(px, py, x, y) * 2 * SCREEN_SCALE_SQUARED / SCREEN_AREA,
        0, 1);
    sound->pan = clamp((x - px) * SCREEN_SCALE / SCREEN_WIDTH, -1, 1);
}

static void shoot(ecs_world_t* world, ecs_entity_t parent, const char* prefab,
                  const Position* player_position, const Position* pposition,
                  const Angle* pangle, const Velocity* pvelocity,
                  Cooldown* pcooldown)
{
    ecs_entity_t projectile_prefab = ecs_lookup(world, prefab);
    ecs_entity_t projectile = ecs_new_w_pair(world, EcsIsA, projectile_prefab);
    ecs_add_pair(world, projectile, EcsChildOf, parent);
    const Velocity* projectile_velocity =
        ecs_get(world, projectile_prefab, Velocity);
    ecs_set_ptr(world, projectile, Position, pposition);
    ecs_set_ptr(world, projectile, Angle, pangle);
    ecs_add(world, projectile, Lifetime);
    ecs_set(world, projectile, Velocity, {
            pvelocity->x + projectile_velocity->x * cos(pangle->angle),
            pvelocity->y + projectile_velocity->y * sin(pangle->angle)});
    SoundInstance s = {.playing = true};
    position_sound(&s, pposition, player_position);
    ecs_set_ptr(world, projectile, SoundInstance, &s);
    pcooldown->current = pcooldown->length;
}

#define JOYSTICK_TOLERANCE 0.1

static void PlayerControl(ecs_iter_t* it)
{
    const Position* position = ecs_field(it, Position, 1);
    Angle* angle = ecs_field(it, Angle, 2);
    const Velocity* velocity = ecs_field(it, Velocity, 3);
    Thrust* thrust = ecs_field(it, Thrust, 4);
    const MaxThrust* max_thrust = ecs_field(it, MaxThrust, 5);
    const MaxTurnSpeed* max_turn_speed = ecs_field(it, MaxTurnSpeed, 6);
    Cooldown* cooldown = ecs_field(it, Cooldown, 7);
    const ExhaustSprite* exhaust_sprite = ecs_field(it, ExhaustSprite, 8);
    ecs_id_t player_id = ecs_field_id(it, 9);
    ecs_entity_t player = ecs_pair_second(it->world, player_id);

    ALLEGRO_KEYBOARD_STATE keyboard;
    al_get_keyboard_state(&keyboard);

    ALLEGRO_JOYSTICK* j = al_get_joystick(0);
    ALLEGRO_JOYSTICK_STATE joystick = {0};
    if(j) al_get_joystick_state(j, &joystick);

    if(al_key_down(&keyboard, ALLEGRO_KEY_A))
    {
        angle->angle -= max_turn_speed->max * it->delta_time;
    }
    if(al_key_down(&keyboard, ALLEGRO_KEY_D))
    {
        angle->angle += max_turn_speed->max * it->delta_time;
    }
    if(al_key_down(&keyboard, ALLEGRO_KEY_W))
    {
        thrust->force += max_thrust->max / exhaust_sprite->levels;
    }
    if((al_key_down(&keyboard, ALLEGRO_KEY_SPACE) || joystick.button[0]) &&
       cooldown->current == 0)
    {
        shoot(it->world, player, "PlayerProjectile", position, position,
              angle, velocity, cooldown);
    }

    float horiz_axis = joystick.stick[0].axis[0];
    float vert_axis = -joystick.stick[0].axis[1];
    if(fabs(horiz_axis) > JOYSTICK_TOLERANCE)
    {
        ecs_assert(fabs(horiz_axis) <= 1, ECS_CONSTRAINT_VIOLATED, NULL);
        angle->angle += max_turn_speed->max * it->delta_time * horiz_axis;
    }
    if(vert_axis > JOYSTICK_TOLERANCE)
    {
        ecs_assert(fabs(vert_axis) <= 1, ECS_CONSTRAINT_VIOLATED, NULL);
        thrust->force += max_thrust->max * vert_axis / exhaust_sprite->levels;
    }
}

#define BUILD_ASSET_PATH(path, filename) \
    int path_size = snprintf(NULL, 0, "%s%s", ASSETS_PATH, filename); \
    char path[path_size + 1];                                         \
    snprintf(path, path_size + 1, "%s%s", ASSETS_PATH, filename);

static void LoadBitmap(ecs_iter_t* it)
{
    Bitmap* bitmap = ecs_field(it, Bitmap, 1);

    for(int i = 0; i < it->count; i++)
    {
        if(UNLIKELY(!bitmap[i].bitmap))
        {
            const char* filename = bitmap[i].name;
            BUILD_ASSET_PATH(path, filename);
            bitmap[i].bitmap = al_load_bitmap(path);
            if(UNLIKELY(!bitmap[i].bitmap))
            {
                report_error("Unable to load bitmap file '%s'", filename);
            }
        }
    }
}

static void LoadSoundData(ecs_iter_t* it)
{
    SoundData* sound_data = ecs_field(it, SoundData, 1);

    for(int i = 0; i < it->count; i++)
    {
        if(UNLIKELY(!sound_data[i].sample))
        {
            const char* filename = sound_data[i].name;
            BUILD_ASSET_PATH(path, filename);
            sound_data[i].sample = al_load_sample(path);
            if(UNLIKELY(!sound_data[i].sample))
            {
                report_error("Unable to load sound file '%s'", filename);
            }
        }
    }
}

static void load_sounds(ecs_iter_t* it, ALLEGRO_PLAYMODE playmode)
{
    const SoundData* sound_data = ecs_field(it, SoundData, 1);
    SoundInstance* sound = ecs_field(it, SoundInstance, 2);

    for(int i = 0; i < it->count; i++)
    {
        if(UNLIKELY(!sound[i].instance))
        {
            ALLEGRO_SAMPLE_INSTANCE* instance = sound[i].instance =
                al_create_sample_instance(sound_data[i].sample);
            sound[i].length = al_get_sample_instance_time(instance);
            al_attach_sample_instance_to_mixer(
                instance, al_get_default_mixer());
            al_set_sample_instance_playmode(instance, playmode);
        }
    }
}

static void LoadSingleSound(ecs_iter_t* it)
{
    load_sounds(it, ALLEGRO_PLAYMODE_ONCE);
}

static void LoadLoopingSound(ecs_iter_t* it)
{
    load_sounds(it, ALLEGRO_PLAYMODE_LOOP);
}

#define FONT_FLAGS (ALLEGRO_TTF_MONOCHROME)

static void LoadFont(ecs_iter_t* it)
{
    Font* font = ecs_field(it, Font, 1);

    for(int i = 0; i < it->count; i++)
    {
        if(UNLIKELY(!font[i].font))
        {
            const char* filename = font[i].name;
            BUILD_ASSET_PATH(path, filename);
            font[i].font = al_load_font(path, font[i].size, FONT_FLAGS);
            if(UNLIKELY(!font[i].font))
            {
                report_error("Unable to load font file '%s'", filename);
            }
        }
    }
}

static void ApplyThrust(ecs_iter_t* it)
{
    const Thrust* thrust = ecs_field(it, Thrust, 1);
    const Angle* angle = ecs_field(it, Angle, 2);
    Velocity* velocity = ecs_field(it, Velocity, 3);

    float dt = it->delta_time;
    for(int i = 0; i < it->count; i++)
    {
        float a = angle[i].angle;
        float thr = thrust[i].force;
        velocity[i].x += dt * thr * cos(a);
        velocity[i].y += dt * thr * sin(a);
    }
}

static void ApplyVelocity(ecs_iter_t* it)
{
    const Velocity* velocity = ecs_field(it, Velocity, 1);
    Position* position = ecs_field(it, Position, 2);

    float dt = it->delta_time;
    for(int i = 0; i < it->count; i++)
    {
        position[i].x += dt * velocity[i].x;
        position[i].y += dt * velocity[i].y;
    }
}

static void RegenerateShield(ecs_iter_t* it)
{
    Shield* shield = ecs_field(it, Shield, 1);

    float dt = it->delta_time;
    for(int i = 0; i < it->count; i++)
    {
        shield[i].strength = clamp(
            shield[i].strength + shield[i].regeneration * dt,
            0, shield[i].max_strength);
    }
}

static void UpdateAnimation(ecs_iter_t* it)
{
    Animation* animation = ecs_field(it, Animation, 1);
    Sprite* sprite = ecs_field(it, Sprite, 2);
    const Size* size = ecs_field(it, Size, 3);

    float dt = it->delta_time;
    for(int i = 0; i < it->count; i++)
    {
        float new_time = animation[i].time + dt;
        uint32_t frame_delta = new_time / animation[i].interval;
        animation[i].time = fmodf(new_time, animation[i].interval);
        uint32_t f = (animation[i].frame + frame_delta) % animation[i].length;
        animation[i].frame = f;
        sprite[i].offset_x = animation[i].first_frame_x + f * size[i].width;
    }
}

static void TickLifetime(ecs_iter_t* it)
{
    Lifetime* lifetime = ecs_field(it, Lifetime, 1);

    float dt = it->delta_time;
    for(int i = 0; i < it->count; i++)
    {
        lifetime[i].left -= dt;
    }
}

static void Expire(ecs_iter_t* it)
{
    Lifetime* lifetime = ecs_field(it, Lifetime, 1);

    for(int i = 0; i < it->count; i++)
    {
        if(UNLIKELY(lifetime[i].left <= 0))
        {
            ecs_delete(it->world, it->entities[i]);
        }
    }
}

static void TickCooldown(ecs_iter_t* it)
{
    Cooldown* cooldown = ecs_field(it, Cooldown, 1);

    float dt = it->delta_time;
    for(int i = 0; i < it->count; i++)
    {
        cooldown[i].current = clamp(cooldown[i].current - dt,
                                    0, cooldown[i].length);
    }
}

static ecs_query_t* collision_query;

static void CollideProjectiles(ecs_iter_t* it)
{
    const Position* position = ecs_field(it, Position, 1);
    const Size* size = ecs_field(it, Size, 2);
    const Damage* damage = ecs_field(it, Damage, 3);
    const Position* player_pos = ecs_field(it, Position, 4);
    ecs_id_t parent_id = ecs_field_id(it, 5);
    ecs_entity_t parent = ecs_pair_second(it->world, parent_id);

    for(int i = 0; i < it->count; i++)
    {
        float x = position[i].x;
        float y = position[i].y;
        float r = fmaxf(size[i].width, size[i].height);

        ecs_iter_t iter = ecs_query_iter(it->world, collision_query);
        while(ecs_query_next(&iter))
        {
            const Position* pos = ecs_field(&iter, Position, 1);
            ecs_id_t entity_id = ecs_field_id(&iter, 5);
            ecs_entity_t entity = ecs_pair_second(it->world, entity_id);

            for(int j = 0; j < iter.count; j++)
            {
                if(UNLIKELY(
                       entity != parent &&
                       distance(pos[j].x, pos[j].y, x, y) <= square(r) * 0.5))
                {
                    Hull* hull = ecs_field(&iter, Hull, 2);
                    Shield* shield = ecs_field(&iter, Shield, 3);
                    SoundInstance* sound = ecs_field(&iter, SoundInstance, 4);

                    float dmg = randint(damage[i].min, damage[i].max);
                    float shield_strength = shield[j].strength;
                    float dmg_after_shield = fmaxf(dmg - shield_strength, 0);
                    shield[j].strength -= fminf(dmg, shield_strength);
                    hull[j].durability -= dmg_after_shield;

                    // shield damage sound
                    al_set_sample_instance_position(sound[j].instance, 0);
                    sound[j].time = 0;
                    sound[j].playing = true;
                    position_sound(&sound[j], &pos[j], player_pos);

                    ecs_delete(it->world, it->entities[i]);
                    goto next_projectile;
                }
            }
        }

    next_projectile:
        continue;
    }
}

static void UpdateAI(ecs_iter_t* it)
{
    const Position* position = ecs_field(it, Position, 1);
    Angle* angle = ecs_field(it, Angle, 2);
    const Shield* shield = ecs_field(it, Shield, 3);
    const Velocity* velocity = ecs_field(it, Velocity, 4);
    Cooldown* cooldown = ecs_field(it, Cooldown, 5);
    const MaxTurnSpeed* max_turn_speed = ecs_field(it, MaxTurnSpeed, 6);
    Thrust* thrust = ecs_field(it, Thrust, 7);
    const MaxThrust* max_thrust = ecs_field(it, MaxThrust, 8);
    const AI* ai = ecs_field(it, AI, 9);
    const Position* player_pos = ecs_field(it, Position, 10);

    float player_x = player_pos->x;
    float player_y = player_pos->y;
    float dt = it->delta_time;
    for(int i = 0; i < it->count; i++)
    {
        float d = distance(position[i].x, position[i].y, player_x, player_y);
        if(d <= square(ai[i].notice_distance))
        {
            float attack_angle = atan2f(player_y - position[i].y,
                                        player_x - position[i].x);

            bool fleeing = shield[i].strength <
                shield[i].max_strength * (1 - ai[i].courage);

            thrust[i].force = fleeing ?
                max_thrust[i].max :
                clamp(max_thrust[i].max * d / square(ai[i].target_distance),
                      0, max_thrust[i].max);
            float target_angle = fleeing? attack_angle + M_PI : attack_angle;
            target_angle = fmodf(target_angle, M_PI * 2);
            float max_turn = max_turn_speed[i].max * dt;
            float delta_angle = clamp(target_angle - angle[i].angle,
                                      -max_turn, max_turn);
            angle[i].angle = angle[i].angle + delta_angle;

            if(cooldown[i].current == 0 &&
               2 * d * sin(0.5 * fabs(attack_angle - angle[i].angle)) <=
               ai[i].shoot_tolerance)
            {
                shoot(it->world, it->entities[i], "EnemyProjectile", player_pos,
                      &position[i], &angle[i], &velocity[i], &cooldown[i]);
            }
        }
        else
        {
            thrust[i].force = 0;
        }
    }
}

static void PositionHUD(ecs_iter_t* it)
{
    const Position* player_pos = ecs_field(it, Position, 1);
    Position* pos = ecs_field(it, Position, 2);
    const Size* size = ecs_field(it, Size, 3);

    const float half_width  = SCREEN_WIDTH  / 2.;
    const float half_height = SCREEN_HEIGHT / 2.;
    pos->x = player_pos->x - (half_width  - size->width  * 2) / SCREEN_SCALE;
    pos->y = player_pos->y + (half_height - size->height * 2) / SCREEN_SCALE;
}

static void DestroyShip(ecs_iter_t* it)
{
    const Position* position = ecs_field(it, Position, 1);
    const Hull* hull = ecs_field(it, Hull, 2);
    const Position* player_pos = ecs_field(it, Position, 3);

    static ecs_entity_t explosion_prefab;
    if(!explosion_prefab)
        explosion_prefab = ecs_lookup(it->world, "ExplosionEffect");

    for(int i = 0; i < it->count; i++)
    {
        if(UNLIKELY(hull[i].durability <= 0))
        {
            ecs_entity_t explosion = ecs_new_w_pair(it->world, EcsIsA,
                                                    explosion_prefab);
            ecs_set_ptr(it->world, explosion, Position, &position[i]);
            ecs_add(it->world, explosion, Animation);
            ecs_add(it->world, explosion, Lifetime);
            SoundInstance s = {.playing = true};
            position_sound(&s, &position[i], player_pos);
            ecs_set_ptr(it->world, explosion, SoundInstance, &s);

            ecs_delete(it->world, it->entities[i]);
        }
    }
}

static void LimitPlayerPosition(ecs_iter_t* it)
{
    const Bitmap* bitmap = ecs_field(it, Bitmap, 1);
    const Background* background = ecs_field(it, Background, 2);
    Position* player_pos = ecs_field(it, Position, 3);
    Velocity* player_velocity = ecs_field(it, Velocity, 4);
    Thrust* player_thrust = ecs_field(it, Thrust, 5);

    float
        player_x = player_pos->x,
        player_y = player_pos->y;

    for(int i = 0; i < it->count; i++)
    {
        ALLEGRO_BITMAP* b = bitmap[i].bitmap;
        float parallax = background[i].parallax;

        float max_x = (al_get_bitmap_width (b) - SCREEN_WIDTH ) * parallax;
        float max_y = (al_get_bitmap_height(b) - SCREEN_HEIGHT) * parallax;

        player_x = clamp(player_x, 0, max_x);
        player_y = clamp(player_y, 0, max_y);
    }

    player_velocity->x = player_x == player_pos->x ? player_velocity->x : 0;
    player_velocity->y = player_y == player_pos->y ? player_velocity->y : 0;

    player_thrust->force =
        player_x == player_pos->x && player_y == player_pos->y ?
        player_thrust->force : 0;

    player_pos->x = player_x;
    player_pos->y = player_y;
}

static void LimitVelocity(ecs_iter_t* it)
{
    Velocity* velocity = ecs_field(it, Velocity, 1);
    const MaxVelocity* max_velocity = ecs_field(it, MaxVelocity, 2);

    for(int i = 0; i < it->count; i++)
    {
        float max = max_velocity[i].max;
        velocity[i].x = clamp(velocity[i].x, -max, max);
        velocity[i].y = clamp(velocity[i].y, -max, max);
    }
}

static void LimitThrust(ecs_iter_t* it)
{
    Thrust* thrust = ecs_field(it, Thrust, 1);
    const MaxThrust* max_thrust = ecs_field(it, MaxThrust, 2);

    for(int i = 0; i < it->count; i++)
    {
        thrust[i].force = clamp(thrust[i].force, 0, max_thrust[i].max);
    }
}

static void SetFuelExhaust(ecs_iter_t* it)
{
    Sprite* sprite = ecs_field(it, Sprite, 1);
    const Size* size = ecs_field(it, Size, 2);
    const ExhaustSprite* exhaust = ecs_field(it, ExhaustSprite, 3);
    const Thrust* thrust = ecs_field(it, Thrust, 4);
    const MaxThrust* max_thrust = ecs_field(it, MaxThrust, 5);

    for(int i = 0; i < it->count; i++)
    {
        float thrust_rate = thrust[i].force / max_thrust[i].max;
        ecs_assert(thrust_rate <= 1, ECS_CONSTRAINT_VIOLATED, NULL);
        uint32_t thrust_level = exhaust[i].levels * thrust_rate;
        sprite[i].offset_y = size[i].height * thrust_level;
    }
}

static void PositionFuelExhaust(ecs_iter_t* it)
{
    Position* position = ecs_field(it, Position, 1);
    Angle* angle = ecs_field(it, Angle, 2);
    const Position* parent_position = ecs_field(it, Position, 3);
    const Size* parent_size = ecs_field(it, Size, 4);
    const Angle* parent_angle = ecs_field(it, Angle, 5);

    for(int i = 0; i < it->count; i++)
    {
        float a = parent_angle[i].angle;
        float r = parent_size[i].height - 1;
        position[i].x = parent_position[i].x - r * cos(a);
        position[i].y = parent_position[i].y - r * sin(a);
        angle[i].angle = a;
    }
}

static void SoundFuelExhaust(ecs_iter_t* it)
{
    const Position* position = ecs_field(it, Position, 1);
    SoundInstance* sound = ecs_field(it, SoundInstance, 2);
    const Thrust* thrust = ecs_field(it, Thrust, 3);
    const MaxThrust* max_thrust = ecs_field(it, MaxThrust, 4);
    const ExhaustSprite* exhaust_sprite = ecs_field(it, ExhaustSprite, 5);
    const Position* player_pos = ecs_field(it, Position, 6);

    for(int i = 0; i < it->count; i++)
    {
        if(thrust[i].force < max_thrust->max / exhaust_sprite->levels)
        {
            sound[i].playing = false;
        }
        else
        {
            sound[i].playing = true;
            position_sound(&sound[i], &position[i], player_pos);
        }
    }
}

static void SetShield(ecs_iter_t* it)
{
    const Shield* shield = ecs_field(it, Shield, 1);
    Sprite* sprite = ecs_field(it, Sprite, 2);
    const ShieldSprite* shield_sprite = ecs_field(it, ShieldSprite, 3);
    const Size* size = ecs_field(it, Size, 4);

    for(int i = 0; i < it->count; i++)
    {
        float shield_strength = shield[i].strength / shield[i].max_strength;
        ecs_assert(shield_strength <= 1, ECS_CONSTRAINT_VIOLATED, NULL);
        uint8_t level = shield_sprite[i].levels * shield_strength;
        sprite[i].offset_x = shield_sprite[i].start_x + size[i].width * level;
    }
}

static void DrawBackground(ecs_iter_t* it)
{
    const Bitmap* bitmap = ecs_field(it, Bitmap, 1);
    const Background* background = ecs_field(it, Background, 2);
    const Position* player_pos = ecs_field(it, Position, 3);

    float
        player_x = player_pos->x,
        player_y = player_pos->y;

    al_hold_bitmap_drawing(true);

    for(int i = 0; i < it->count; i++)
    {
        float parallax = background[i].parallax;
        al_draw_bitmap_region(
            bitmap[i].bitmap,
            player_x / parallax,
            player_y / parallax,
            SCREEN_WIDTH,
            SCREEN_HEIGHT,
            0, 0, 0);
    }
}

static void DrawSprite(ecs_iter_t* it)
{
    const Bitmap* bitmap = ecs_field(it, Bitmap, 1);
    const Sprite* sprite = ecs_field(it, Sprite, 2);
    const Size* size = ecs_field(it, Size, 3);
    const Position* position = ecs_field(it, Position, 4);
    const Angle* angle = ecs_field(it, Angle, 5);
    const Position* player_pos = ecs_field(it, Position, 6);

    float off_x = SCREEN_WIDTH / (2. * SCREEN_SCALE) - player_pos->x;
    float off_y = SCREEN_HEIGHT / (2. * SCREEN_SCALE) - player_pos->y;

    ALLEGRO_COLOR tint = al_map_rgba(255, 255, 255, 255);

    for(int i = 0; i < it->count; i++)
    {
        float w = size[i].width;
        float h = size[i].height;
        float x = (position[i].x + off_x) * SCREEN_SCALE;
        float y = (position[i].y + off_y) * SCREEN_SCALE;
        if(UNLIKELY(x > -w && y > -h &&
                    x < SCREEN_WIDTH + w && y < SCREEN_HEIGHT + h))
            al_draw_tinted_scaled_rotated_bitmap_region(
                bitmap[i].bitmap,
                sprite[i].offset_x,
                sprite[i].offset_y,
                w,
                h,
                tint,
                w / 2,
                h / 2,
                x,
                y,
                SCREEN_SCALE,
                SCREEN_SCALE,
                angle[i].angle + M_PI_2,
                0);
    }
}

static void DrawEffect(ecs_iter_t* it)
{
    const Bitmap* bitmap = ecs_field(it, Bitmap, 1);
    const Sprite* sprite = ecs_field(it, Sprite, 2);
    const Size* size = ecs_field(it, Size, 3);
    const Position* position = ecs_field(it, Position, 4);
    const Position* player_pos = ecs_field(it, Position, 6);

    float off_x = SCREEN_WIDTH / (2. * SCREEN_SCALE) - player_pos->x;
    float off_y = SCREEN_HEIGHT / (2. * SCREEN_SCALE) - player_pos->y;

    for(int i = 0; i < it->count; i++)
    {
        float w = size[i].width;
        float h = size[i].height;
        float x = (position[i].x + off_x - size[i].width  / 2) * SCREEN_SCALE;
        float y = (position[i].y + off_y - size[i].height / 2) * SCREEN_SCALE;
        if(UNLIKELY(x > -w && y > -h &&
                    x < SCREEN_WIDTH + w && y < SCREEN_HEIGHT + h))
            al_draw_scaled_bitmap(
                bitmap[i].bitmap,
                sprite[i].offset_x,
                sprite[i].offset_y,
                w,
                h,
                x,
                y,
                w * SCREEN_SCALE,
                h * SCREEN_SCALE,
                0);
    }
}

static void DrawText(ecs_iter_t* it)
{
    const Position* position = ecs_field(it, Position, 1);
    const Font* font = ecs_field(it, Font, 2);
    const Text* text = ecs_field(it, Text, 3);

    ALLEGRO_COLOR color = al_map_rgb(255, 255, 255);
    for(int i = 0; i < it->count; i++)
    {
        al_draw_multiline_text(
            font[i].font,
            color,
            position[i].x,
            position[i].y,
            SCREEN_WIDTH,
            0,
            ALLEGRO_ALIGN_CENTRE,
            text[i].string);
    }
}

static void SetHullHUDText(ecs_iter_t* it)
{
    const Hull* hull = ecs_field(it, Hull, 1);
    Text* text = ecs_field(it, Text, 2);

    unsigned percentage = clamp(100 * hull->durability / hull->max_durability,
                                0, 100);
    sprintf(text->string, "%d%%", percentage);
}

static void SetEnemiesHUDText(ecs_iter_t* it)
{
    Text* text = ecs_field(it, Text, 1);
    unsigned enemies = ecs_count_id(it->world, Enemy);
    sprintf(text->string, enemies == 1 ? "%d enemy" : "%d enemies", enemies);
}

#define MARKER_COLOR {1, 0, 0, 0}

static void DrawTargetMarker(ecs_iter_t* it)
{
    al_hold_bitmap_drawing(false);

    const Position* position = ecs_field(it, Position, 1);
    const Size* size = ecs_field(it, Size, 2);

    ecs_entity_t player = ecs_lookup(it->world, "player");
    const Position* player_pos = ecs_get(it->world, player, Position);

    const float half_width  = SCREEN_WIDTH  / (2. * SCREEN_SCALE);
    const float half_height = SCREEN_HEIGHT / (2. * SCREEN_SCALE);

    size_t nverts = it->count * 2;
    ALLEGRO_VERTEX vertices[nverts];
    memset(vertices, 0, nverts * sizeof(ALLEGRO_VERTEX));

    for(int i = 0; i < it->count; i++)
    {
        float diff_x = player_pos->x - position[i].x;
        float diff_y = player_pos->y - position[i].y;
        float dx = fabs(diff_x);
        float dy = fabs(diff_y);
        float width = size[i].width;
        float height = size[i].height;
        if(LIKELY(dx - width > half_width || dy - height > half_height))
        {
            float mx = half_width  - copysignf(fminf(dx, half_width ), diff_x);
            float my = half_height - copysignf(fminf(dy, half_height), diff_y);
            float x1 = mx * SCREEN_SCALE;
            float y1 = my * SCREEN_SCALE;
            float angle = atan2f(diff_y, diff_x);
            float x2 = width  * cos(angle);
            float y2 = height * sin(angle);

            vertices[i * 2].x = x1;
            vertices[i * 2].y = y1;
            vertices[i * 2].color = (ALLEGRO_COLOR) MARKER_COLOR;

            vertices[i * 2 + 1].x = x1 + x2;
            vertices[i * 2 + 1].y = y1 + y2;
            vertices[i * 2 + 1].color = (ALLEGRO_COLOR) MARKER_COLOR;
        }
    }

    al_draw_prim(vertices, NULL, NULL, 0, nverts + 1, ALLEGRO_PRIM_LINE_LIST);
}

static void TickSingleSound(ecs_iter_t* it)
{
    SoundInstance* sound = ecs_field(it, SoundInstance, 1);

    float dt = it->delta_time;
    for(int i = 0; i < it->count; i++)
    {
        if(UNLIKELY(sound[i].playing))
        {
            sound[i].time += dt;
            if(UNLIKELY(sound[i].time > sound[i].length))
            {
                sound[i].time = 0;
                sound[i].playing = false;
            }
        }
    }
}

static void SetSoundParameters(ecs_iter_t* it)
{
    const SoundInstance* sound = ecs_field(it, SoundInstance, 1);

    for(int i = 0; i < it->count; i++)
    {
        ALLEGRO_SAMPLE_INSTANCE* spl = sound[i].instance;
        bool playing = sound[i].playing;
        if(UNLIKELY(playing != al_get_sample_instance_playing(spl)))
        {
            al_set_sample_instance_playing(spl, playing);
        }
        if(UNLIKELY(playing))
        {
            al_set_sample_instance_gain(spl, sound[i].gain);
            al_set_sample_instance_pan(spl, sound[i].pan);
        }
    }
}

static void StopSound(ecs_iter_t* it)
{
    SoundInstance* sound = ecs_field(it, SoundInstance, 1);

    for(int i = 0; i < it->count; i++)
    {
        if(LIKELY(sound[i].instance))
            al_stop_sample_instance(sound[i].instance);
    }
}

static inline void show_text(ecs_world_t* world, const char* entity)
{
    ecs_entity_t text = ecs_lookup(world, entity);
    ecs_set(world, text, Position, {SCREEN_WIDTH / 2., 0});
}

static void WinCondition(ecs_iter_t* it)
{
    if(ecs_count_id(it->world, Enemy) == 1)
    {
        DELETE_SYSTEM(it->world, PlayerControl);
        DELETE_SYSTEM(it->world, DrawBackground);

        show_text(it->world, "win");
    }
}

static void FailCondition(ecs_iter_t* it)
{
    for(int i = 0; i < it->count; i++)
    {
        if(ecs_has_id(it->world, it->entities[i], Player))
        {
            DELETE_SYSTEM(it->world, PlayerControl);
            DELETE_SYSTEM(it->world, DrawBackground);
            DELETE_SYSTEM(it->world, SoundFuelExhaust);
            DELETE_SYSTEM(it->world, UpdateAI);
            DELETE_SYSTEM(it->world, LimitPlayerPosition);
            DELETE_SYSTEM(it->world, DrawBackground);
            DELETE_SYSTEM(it->world, DrawSprite);
            DELETE_SYSTEM(it->world, DrawEffect);
            DELETE_SYSTEM(it->world, DrawTargetMarker);
            DELETE_SYSTEM(it->world, PositionHUD);
            DELETE_SYSTEM(it->world, SetHullHUDText);
            DELETE_SYSTEM(it->world, CollideProjectiles);
            DELETE_SYSTEM(it->world, DestroyShip);

            ecs_delete_with(it->world, ecs_id(SoundInstance));

            show_text(it->world, "fail");

            return;
        }
    }
}

void register_systems(ecs_world_t* world)
{
    // https://tinyurl.com/flecs-phases

    ECS_SYSTEM(world, GenerateFleet, EcsOnLoad,
               [in] Fleet);

    ECS_SYSTEM(world, DecayPlayerThrust, EcsPostLoad,
               Thrust, [in] MaxThrust, [in] Player);
    ECS_SYSTEM(world, PlayerControl, EcsPostLoad,
               [in] Position(parent), Angle(parent), [in] Velocity(parent),
               Thrust(parent), [in] MaxThrust(parent),
               [in] MaxTurnSpeed(parent), Cooldown(parent), [in] ExhaustSprite,
               (EcsChildOf, *), Player(parent));

    ECS_SYSTEM(world, LoadBitmap, EcsPreUpdate,
               Bitmap);
    ECS_SYSTEM(world, LoadSoundData, EcsPreUpdate,
               SoundData);
    ECS_SYSTEM(world, LoadSingleSound, EcsPreUpdate,
               [in] SoundData, SoundInstance, SingleSound);
    ECS_SYSTEM(world, LoadLoopingSound, EcsPreUpdate,
               [in] SoundData, SoundInstance, LoopingSound);
    ECS_SYSTEM(world, LoadFont, EcsPreUpdate,
               Font);

    ECS_SYSTEM(world, ApplyThrust, EcsOnUpdate,
               [in] Thrust, [in] Angle, Velocity);
    ECS_SYSTEM(world, ApplyVelocity, EcsOnUpdate,
               [in] Velocity, Position);
    ECS_SYSTEM(world, RegenerateShield, EcsOnUpdate,
               Shield);
    ECS_SYSTEM(world, UpdateAnimation, EcsOnUpdate,
               Animation, Sprite, [in] Size);
    ECS_SYSTEM(world, TickLifetime, EcsOnUpdate,
               Lifetime);
    ECS_SYSTEM(world, Expire, EcsOnUpdate,
               Lifetime);
    ECS_SYSTEM(world, TickCooldown, EcsOnUpdate,
               Cooldown);
    ECS_SYSTEM(world, CollideProjectiles, EcsOnUpdate,
               [in] Position, [in] Size, [in] Damage, [in] Position(player),
               (EcsChildOf, *));
    ECS_SYSTEM(world, UpdateAI, EcsOnUpdate,
               [in] Position, Angle, [in] Shield, [in] Velocity, Cooldown,
               [in] MaxTurnSpeed, Thrust, [in] MaxThrust, [in] AI,
               [in] Position(player));
    ECS_SYSTEM(world, PositionHUD, EcsOnUpdate,
               [in] Position(player), Position(hullIcon), [in] Size(hullIcon));

    ECS_SYSTEM(world, DestroyShip, EcsOnValidate,
               [in] Position, [in] Hull, [in] Position(player));
    ECS_SYSTEM(world, LimitPlayerPosition, EcsOnValidate,
               [in] Bitmap, [in] Background, Position(player), Velocity(player),
               Thrust(player));
    ECS_SYSTEM(world, LimitVelocity, EcsOnValidate,
               Velocity, [in] MaxVelocity);
    ECS_SYSTEM(world, LimitThrust, EcsOnValidate,
               Thrust, [in] MaxThrust);

    ECS_SYSTEM(world, SetFuelExhaust, EcsOnStore,
               Sprite, [in] Size, [in] ExhaustSprite, [in] Thrust(parent),
               [in] MaxThrust(parent));
    ECS_SYSTEM(world, PositionFuelExhaust, EcsOnStore,
               Position, Angle, [in] Position(parent), [in] Size(parent),
               [in] Angle(parent), [in] ExhaustSprite);
    ECS_SYSTEM(world, SoundFuelExhaust, EcsOnStore,
               [in] Position, SoundInstance, [in] Thrust(parent),
               [in] MaxThrust(parent), [in] ExhaustSprite,
               [in] Position(player));
    ECS_SYSTEM(world, SetShield, EcsOnStore,
               [in] Shield(parent), Sprite, [in] ShieldSprite, [in] Size);
    ECS_SYSTEM(world, DrawBackground, EcsOnStore,
               [in] Bitmap, [in] Background, [in] Position(player));
    ECS_SYSTEM(world, DrawSprite, EcsOnStore,
               [in] Bitmap, [in] Sprite, [in] Size, [in] Position, [in] Angle,
               [in] Position(player));
    ECS_SYSTEM(world, DrawEffect, EcsOnStore,
               [in] Bitmap, [in] Sprite, [in] Size, [in] Position(self|parent),
               !Angle, [in] Position(player));
    ECS_SYSTEM(world, DrawText, EcsOnStore,
               [in] Position, [in] Font, [in] Text);
    ECS_SYSTEM(world, SetHullHUDText, EcsOnStore,
               [in] Hull(player), Text(hullHUD));
    ECS_SYSTEM(world, SetEnemiesHUDText, EcsOnStore,
               Text(enemiesHUD));
    ECS_SYSTEM(world, DrawTargetMarker, EcsOnStore,
               [in] Position, [in] Size, [in] Hull);
    ECS_SYSTEM(world, TickSingleSound, EcsOnStore,
               SoundInstance, SingleSound);
    ECS_SYSTEM(world, SetSoundParameters, EcsOnStore,
               [in] SoundInstance);

    ECS_OBSERVER(world, StopSound, EcsOnRemove, SoundInstance);
    ECS_OBSERVER(world, WinCondition, EcsOnRemove, Hull);
    ECS_OBSERVER(world, FailCondition, EcsOnRemove, Hull);

    collision_query = ecs_query(world, {
            .filter.expr = "[in] Position(parent), Hull(parent), "
            "Shield(parent), SoundInstance, [in] (EcsChildOf, *), "
            "[in] ShieldSprite",
            .entity = ecs_new_entity(world, "CollisionQuery")});
}
