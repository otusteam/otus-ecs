cmake_minimum_required(VERSION 3.13)

if(POLICY CMP0077)
  set(CMAKE_POLICY_DEFAULT_CMP0077 NEW)
endif(POLICY CMP0077)

project(otus-ecs C)

# Set compilation options
set(CMAKE_C_STANDARD 11)
set(CMAKE_C_STANDARD_REQUIRED TRUE)
add_compile_options(-march=x86-64 -mtune=generic
  -mmmx -msse -msse2 -msse3 -mssse3 -mcx16 -msahf -mfxsr
  $<$<NOT:$<CONFIG:Debug>>:-flto>
  $<$<NOT:$<CONFIG:Debug>>:-ffast-math>
  $<$<NOT:$<CONFIG:Debug>>:-fno-stack-protector>
  $<$<CONFIG:RELEASE>:-fomit-frame-pointer>)

# Find liballegro
find_package(PkgConfig REQUIRED)
pkg_check_modules(ALLEGRO REQUIRED
  allegro-5
  allegro_acodec-5
  allegro_audio-5
  allegro_dialog-5
  allegro_font-5
  allegro_image-5
  allegro_main-5
  allegro_primitives-5
  allegro_ttf-5)

# Ensure git submodule updated
find_package(Git QUIET)
if(GIT_FOUND AND EXISTS "${PROJECT_SOURCE_DIR}/.git")
  option(GIT_SUBMODULE "Check submodules during build" ON)
  if(GIT_SUBMODULE)
    message(STATUS "Updating Git submodules")
    execute_process(
      COMMAND ${GIT_EXECUTABLE} submodule update --init --recursive
      WORKING_DIRECTORY ${CMAKE_CURRENT_SOURCE_DIR}
      RESULT_VARIABLE GIT_RESULT)
    if(NOT GIT_RESULT EQUAL "0")
      message(STATUS "Updating Git submodules failed with ${GIT_RESULT}")
    endif()
  endif()
endif()
if(NOT EXISTS "${PROJECT_SOURCE_DIR}/contrib/flecs/CMakeLists.txt")
  message(FATAL_ERROR
    "Missing flecs submodule. Please update git submodules and try again.")
endif()

# Build Flecs
set(FLECS_PIC OFF)
set(FLECS_SHARED OFF)
set(RELEASE_DEFINES "-DFLECS_CUSTOM_BUILD -DFLECS_SYSTEM \
-DFLECS_META_C -DFLECS_EXPR -DFLECS_PIPELINE -DFLECS_PLECS \
-U_FORTIFY_SOURCE -D_FORTIFY_SOURCE=0")
set(CMAKE_C_FLAGS_RELEASE
  "${CMAKE_C_FLAGS_RELEASE} ${RELEASE_DEFINES}")
set(CMAKE_C_FLAGS_RELWITHDEBINFO
  "${CMAKE_C_FLAGS_RELWITHDEBINFO} ${RELEASE_DEFINES}")
add_subdirectory(contrib/flecs EXCLUDE_FROM_ALL)
set_target_properties(flecs_static PROPERTIES INTERFACE_COMPILE_DEFINITIONS "")
include_directories(contrib/flecs)

# Build executable
set(SOURCES
  src/components.c
  src/systems.c
  src/main.c
  src/utils.c)
add_executable(otus-ecs WIN32 ${SOURCES})
target_include_directories(otus-ecs PRIVATE ${ALLEGRO_INCLUDE_DIRS})
target_compile_options(otus-ecs PRIVATE -Wall -Wextra -Wpedantic)
target_link_libraries(otus-ecs m ${ALLEGRO_LIBRARIES} flecs_static)
target_link_options(otus-ecs PRIVATE
  $<$<NOT:$<CONFIG:Debug>>:-flto> $<$<CONFIG:RELEASE>:-s>)

# Pack release
if(MINGW)
  pkg_get_variable(ALLEGRO_EXEC_PREFIX allegro-5 exec_prefix)
  set(GCCVER ${CMAKE_C_COMPILER_VERSION})
  install(FILES
    ${ALLEGRO_EXEC_PREFIX}/bin/allegro-5.2.dll
    ${ALLEGRO_EXEC_PREFIX}/bin/allegro_acodec-5.2.dll
    ${ALLEGRO_EXEC_PREFIX}/bin/allegro_audio-5.2.dll
    ${ALLEGRO_EXEC_PREFIX}/bin/allegro_dialog-5.2.dll
    ${ALLEGRO_EXEC_PREFIX}/bin/allegro_font-5.2.dll
    ${ALLEGRO_EXEC_PREFIX}/bin/allegro_image-5.2.dll
    ${ALLEGRO_EXEC_PREFIX}/bin/allegro_primitives-5.2.dll
    ${ALLEGRO_EXEC_PREFIX}/bin/allegro_ttf-5.2.dll
    ${ALLEGRO_EXEC_PREFIX}/bin/libwinpthread-1.dll
    ${ALLEGRO_EXEC_PREFIX}/bin/zlib1.dll
    /usr/lib/gcc/x86_64-w64-mingw32/${GCCVER}/libgcc_s_seh-1.dll
    /usr/lib/gcc/x86_64-w64-mingw32/${GCCVER}/libstdc++-6.dll
    DESTINATION bin)
endif()
install(TARGETS otus-ecs RUNTIME DESTINATION bin)
install(DIRECTORY Resources DESTINATION .)
set(CPACK_GENERATOR "ZIP")
include(CPack)
